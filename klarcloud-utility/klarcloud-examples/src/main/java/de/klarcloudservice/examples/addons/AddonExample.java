/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.examples.addons;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.event.Listener;
import de.klarcloudservice.event.enums.EventTargetType;
import de.klarcloudservice.event.events.IncomingPacketEvent;
import de.klarcloudservice.event.events.LoadSuccessEvent;
import de.klarcloudservice.event.events.ProcessUnregistersEvent;
import de.klarcloudservice.utility.ControllerAddonImpl;

/**
 * @author _Klaro | Pasqual K. / created on 27.12.2018
 */

public class AddonExample extends ControllerAddonImpl {
    @Override
    public void onModuleClazzPrepare() {
        //Called when Addon main class is prepared and configuration is ready
        //Loads the Addon, Cloud is still loading
        this.getInternalKlarCloudSystem().getEventManager().registerListener(new Listener("A cool Event", EventTargetType.LOAD_SUCCESS) {
            @Override
            public void handle(LoadSuccessEvent event) {
                KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Already loaded this cloud?!");
            }
        });
    }

    @Override
    public void onModuleLoading() {
        //Called when Cloud is finished with loading all other stuff
        //Enables the Addon, Cloud is now ready to use
        this.getInternalKlarCloudSystem().getEventManager().registerListener(new Listener("Another cool event", EventTargetType.INCOMING_PACKET) {
            @Override
            public void handle(IncomingPacketEvent event) {
                KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Is there a packet incoming (" + event.getPacket().getPacketSender().name() + ")");
            }
        });
    }

    @Override
    public void onModuleReadyToClose() {
        //Called when Cloud stops or get reloaded
        //Starts again by onModuleClazzPrepare() if cloud gets reloaded
        this.getInternalKlarCloudSystem().getEventManager().registerListener(new Listener("Close Event", EventTargetType.PROCESS_UNREGISTERED) {
            @Override
            public void handle(ProcessUnregistersEvent event) {
                KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("HUH? Where is my process? " + event.getName());
            }
        });
    }
}
