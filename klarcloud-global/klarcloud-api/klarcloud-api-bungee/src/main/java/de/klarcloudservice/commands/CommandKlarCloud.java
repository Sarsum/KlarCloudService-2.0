/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudAPIBungee;
import de.klarcloudservice.netty.packets.PacketOutDispatchConsoleCommand;
import de.klarcloudservice.utility.StringUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.util.Arrays;

/**
 * @author _Klaro | Pasqual K. / created on 16.12.2018
 */

public final class CommandKlarCloud extends Command {
    public CommandKlarCloud() {
        super("klarcloud");
    }

    @Override
    public final String getPermission() {
        return "klarcloud.command.klarcloud";
    }

    @Override
    public final String[] getAliases() {
        return new String[]{"kc", "klarc", "kcloud"};
    }

    @Override
    public void execute(final CommandSender commandSender, final String[] strings) {
        if (!commandSender.hasPermission("klarcloud.command.klarcloud")) {
            commandSender.sendMessage(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-command-no-permission")));
            return;
        }

        if (strings.length == 0) {
            final String prefix = KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getPrefix() + "§7";

            commandSender.sendMessage(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-command-klarcloud-invalid-syntax")));
            commandSender.sendMessage(
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud process <start/stop> <name> \n")),
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud clear \n")),
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud update \n")),
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud whitelist <add/remove> <proxyGroup/--all> <name> \n")),
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud execute <server/proxy> <name> <command> \n")),
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud delete <servergroup/proxygroup/client> <name> \n")),
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud exit \n")),
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud reload\n")),
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud create client <name> <ip> \n")),
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud create servergroup <name> <client> \n")),
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud create proxygroup <name> <client> <host> \n")),
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud copy <serverName> \n")),
                    new TextComponent(TextComponent.fromLegacyText(prefix + "/klarcloud info"))
            );
            return;
        }

        if (!commandSender.hasPermission("klarcloud.command." + strings[0])) {
            commandSender.sendMessage(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-command-klarcloud-no-permission")));
            return;
        }

        if (strings[0].equalsIgnoreCase("info")) {
            commandSender.sendMessage(TextComponent.fromLegacyText("You are using the KlarCloudVersion " + StringUtil.KLARCLOUD_VERSION + "@" + StringUtil.KLARCLOUD_SPECIFICATION + " by KlarCloudService.de"));
            return;
        }

        StringBuilder stringBuilder = new StringBuilder();
        Arrays.stream(strings).forEach(e -> stringBuilder.append(e).append(StringUtil.SPACE));

        KlarCloudAPIBungee.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new PacketOutDispatchConsoleCommand(stringBuilder.substring(0, stringBuilder.length() - 1)));
        commandSender.sendMessage(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-command-klarcloud-command-success")));
    }
}
