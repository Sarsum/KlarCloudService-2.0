/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.listener;

import de.klarcloudservice.KlarCloudAPIBungee;
import de.klarcloudservice.bootstrap.BungeecordBootstrap;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.meta.proxy.configuration.ProxyConfig;
import de.klarcloudservice.meta.proxy.configuration.ProxyMaintenanceConfig;
import de.klarcloudservice.utility.StringUtil;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 02.11.2018
 */

public final class CloudProxyPingListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(final ProxyPingEvent event) {
        final ProxyGroup proxyGroup = KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getProxyGroups().get(KlarCloudAPIBungee.getInstance().getProxyInfo().getProxyGroup().getName());
        if (proxyGroup == null) return;
        final ServerPing response = event.getResponse();
        final ProxyConfig proxyConfig = proxyGroup.getProxyConfig();
        final ProxyMaintenanceConfig maintenanceConfig = proxyConfig.getProxyMaintenanceConfig();
        if (proxyConfig.isMaintenance()) {
            response.setDescriptionComponent(new TextComponent(TextComponent.fromLegacyText(
                    ChatColor.translateAlternateColorCodes('&', maintenanceConfig.getMaintenanceMode().getLineOne() + "\n"
                            + maintenanceConfig.getMaintenanceMode().getLineTwo())
                            .replace("%version%", StringUtil.KLARCLOUD_VERSION + "@" + StringUtil.KLARCLOUD_SPECIFICATION)
                            .replace("%proxy%", KlarCloudAPIBungee.getInstance().getProxyInfo().getCloudProcess().getName())
            )));
            response.setPlayers(new ServerPing.Players(
                    proxyConfig.getMaxPlayers(),
                    BungeecordBootstrap.getInstance().getProxy().getPlayers().size(),
                    this.stringListToPlayerInfoArray(maintenanceConfig.getMaintenancePlayerInfo())
            ));
            response.setVersion(new ServerPing.Protocol(maintenanceConfig.getMaintenanceProtocol()
                    .replace("%max%", proxyConfig.getMaxPlayers() + "")
                    .replace("%online%", BungeecordBootstrap.getInstance().getProxy().getPlayers().size() + ""),
                    - 1));
        } else {
            if (proxyConfig.getNormalLayout().isEnabled())
                response.setDescriptionComponent(new TextComponent(TextComponent.fromLegacyText(
                        ChatColor.translateAlternateColorCodes('&', proxyConfig.getNormalLayout().getLineOne() + "\n"
                                + proxyConfig.getNormalLayout().getLineTwo())
                                .replace("%version%", StringUtil.KLARCLOUD_VERSION + "@" + StringUtil.KLARCLOUD_SPECIFICATION)
                                .replace("%proxy%", KlarCloudAPIBungee.getInstance().getProxyInfo().getCloudProcess().getName())
                )));
            response.setPlayers(new ServerPing.Players(
                    proxyConfig.getMaxPlayers(),
                    BungeecordBootstrap.getInstance().getProxy().getPlayers().size(),
                    this.stringListToPlayerInfoArray(proxyConfig.getPlayerInfo())
            ));
            if (proxyConfig.isCustomProtocol())
                response.setVersion(new ServerPing.Protocol(proxyConfig.getCustomProtocolText()
                        .replace("%max%", proxyConfig.getMaxPlayers() + "")
                        .replace("%online%", BungeecordBootstrap.getInstance().getProxy().getPlayers().size() + "")
                        , - 1));
        }

        event.setResponse(response);
    }

    private ServerPing.PlayerInfo[] stringListToPlayerInfoArray(List<String> stringList) {
        ServerPing.PlayerInfo[] playerInfos = new ServerPing.PlayerInfo[stringList.size()];
        for (short i = 0; i < playerInfos.length; i++)
            playerInfos[i] = new ServerPing.PlayerInfo(ChatColor.translateAlternateColorCodes('&', stringList.get(i)), UUID.randomUUID());
        return playerInfos;
    }
}
