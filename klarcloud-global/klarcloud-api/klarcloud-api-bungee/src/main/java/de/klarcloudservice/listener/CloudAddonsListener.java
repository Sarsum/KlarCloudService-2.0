/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.listener;

import de.klarcloudservice.KlarCloudAPIBungee;
import de.klarcloudservice.netty.packets.PacketOutCommandExecute;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * @author _Klaro | Pasqual K. / created on 07.11.2018
 */

public final class CloudAddonsListener implements Listener {
    @EventHandler
    public void handle(final ChatEvent event) {
        if (!(event.getSender() instanceof ProxiedPlayer)) return;
        final ProxiedPlayer proxiedPlayer = (ProxiedPlayer) event.getSender();

        if (event.isCommand() && !event.isCancelled() && KlarCloudAPIBungee.getInstance().getProxyInfo().getProxyGroup().getProxyConfig().isControllerCommandLogging())
            KlarCloudAPIBungee.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new PacketOutCommandExecute(proxiedPlayer.getName(), proxiedPlayer.getUniqueId(), event.getMessage()));
    }
}
