/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packets;

import de.klarcloudservice.KlarCloudAPISpigot;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.Collections;

/**
 * @author _Klaro | Pasqual K. / created on 12.01.2019
 */

public final class PacketOutRequestSignUpdate extends Packet {
    public PacketOutRequestSignUpdate() {
        super("RequestSignUpdate", new Configuration().addStringProperty("name", KlarCloudAPISpigot.getInstance().getServerInfo().getCloudProcess().getName()), Collections.singletonList(QueryType.COMPLETE), PacketSender.PROCESS_SERVER);
    }
}
