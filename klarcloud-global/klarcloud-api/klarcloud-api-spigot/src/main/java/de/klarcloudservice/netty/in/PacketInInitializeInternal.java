/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudAPISpigot;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.signaddon.SignSelector;
import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.utility.TypeTokenAdaptor;

import java.util.Collections;
import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 09.12.2018
 */

public class PacketInInitializeInternal implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        if (!queryTypes.contains(QueryType.RESULT) || queryTypes.contains(QueryType.NO_RESULT)) return;

        KlarCloudAPISpigot.getInstance().setInternalCloudNetwork(configuration.getValue("networkProperties", TypeTokenAdaptor.getInternalCloudNetworkType()));
        KlarCloudAPISpigot.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new Packet(
                "AuthSuccess", new Configuration().addStringProperty("name", KlarCloudAPISpigot.getInstance().getServerInfo().getCloudProcess().getName()),
                Collections.singletonList(QueryType.COMPLETE), PacketSender.PROCESS_SERVER
        ));

        if (KlarCloudAPISpigot.getInstance().getInternalCloudNetwork().isSigns())
            try {
                new SignSelector();
            } catch (final Throwable throwable) {
                StringUtil.printError(KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger(), "Error while creating signSelector instance", throwable);
            }
    }
}
