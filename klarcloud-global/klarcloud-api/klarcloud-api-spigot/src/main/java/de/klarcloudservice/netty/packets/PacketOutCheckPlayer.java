/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packets;

import de.klarcloudservice.KlarCloudAPISpigot;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.Collections;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 14.12.2018
 */

public final class PacketOutCheckPlayer extends Packet {

    public PacketOutCheckPlayer(final UUID uuid) {
        super("PlayerAccepted", new Configuration().addStringProperty("name", KlarCloudAPISpigot.getInstance().getServerInfo().getCloudProcess().getName()).addProperty("uuid", uuid), Collections.singletonList(QueryType.COMPLETE), PacketSender.PROCESS_SERVER);
    }
}
