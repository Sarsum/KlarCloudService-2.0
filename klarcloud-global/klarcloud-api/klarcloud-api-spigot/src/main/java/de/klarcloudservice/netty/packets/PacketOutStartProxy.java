/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packets;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.Collections;

/**
 * @author _Klaro | Pasqual K. / created on 26.12.2018
 */

public final class PacketOutStartProxy extends Packet {
    public PacketOutStartProxy(final ProxyGroup proxyGroup, final Configuration preConfig) {
        super("StartProxyProcess", new Configuration().addProperty("group", proxyGroup).addConfigurationProperty("pre", preConfig), Collections.singletonList(QueryType.COMPLETE), PacketSender.PROCESS_SERVER);
    }
}
