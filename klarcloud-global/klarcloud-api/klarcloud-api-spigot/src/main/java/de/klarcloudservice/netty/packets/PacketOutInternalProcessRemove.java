/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packets;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.authentication.enums.AuthenticationType;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.Collections;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 09.12.2018
 */

public final class PacketOutInternalProcessRemove extends Packet {
    public PacketOutInternalProcessRemove(final UUID processUID, final AuthenticationType authenticationType) {
        super("InternalProcessRemove", new Configuration().addProperty("uid", processUID).addStringProperty("type", authenticationType.name()), Collections.singletonList(QueryType.COMPLETE), PacketSender.PROCESS_SERVER);
    }
}
