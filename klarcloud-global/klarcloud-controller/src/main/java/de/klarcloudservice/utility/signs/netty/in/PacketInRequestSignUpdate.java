/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility.signs.netty.in;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.signs.SignSelector;
import de.klarcloudservice.utility.signs.netty.packets.PacketOutSendSigns;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 12.01.2019
 */

public class PacketInRequestSignUpdate implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(configuration.getStringValue("name"), new PacketOutSendSigns(SignSelector.getInstance().getSignConfiguration().getSignLayoutConfiguration(), SignSelector.getInstance().getSignConfiguration().getSignMap()));
    }
}
