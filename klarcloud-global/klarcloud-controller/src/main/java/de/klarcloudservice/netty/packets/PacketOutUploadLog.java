/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packets;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.io.Serializable;
import java.util.Collections;

/**
 * @author _Klaro | Pasqual K. / created on 29.01.2019
 */

public final class PacketOutUploadLog extends Packet implements Serializable {
    private static final long serialVersionUID = -3275070800933988588L;

    public PacketOutUploadLog(final String name, final String type) {
        super(
                "PacketInUploadLog",
                new Configuration().addStringProperty("name", name).addStringProperty("type", type),
                Collections.singletonList(QueryType.COMPLETE),
                PacketSender.CONTROLLER
        );
    }
}
