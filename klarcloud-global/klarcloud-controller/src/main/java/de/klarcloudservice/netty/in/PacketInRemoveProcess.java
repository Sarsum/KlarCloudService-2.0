/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.event.enums.EventTargetType;
import de.klarcloudservice.event.events.ProcessUnregistersEvent;
import de.klarcloudservice.meta.info.ProxyInfo;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.netty.packets.PacketOutProcessRemove;
import de.klarcloudservice.utility.TypeTokenAdaptor;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 11.11.2018
 */

public class PacketInRemoveProcess implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        if (configuration.contains("serverInfo")) {
            final ServerInfo serverInfo = configuration.getValue("serverInfo", TypeTokenAdaptor.getServerInfoType());

            KlarCloudController.getInstance().getEventManager().callEvent(EventTargetType.PROCESS_UNREGISTERED, new ProcessUnregistersEvent(serverInfo.getCloudProcess().getName()));

            KlarCloudController.getInstance().getChannelHandler().sendToAllAsynchronous(new PacketOutProcessRemove(serverInfo));
        } else {
            final ProxyInfo proxyInfo = configuration.getValue("proxyInfo", TypeTokenAdaptor.getProxyInfoType());

            KlarCloudController.getInstance().getEventManager().callEvent(EventTargetType.PROCESS_UNREGISTERED, new ProcessUnregistersEvent(proxyInfo.getCloudProcess().getName()));

            KlarCloudController.getInstance().getChannelHandler().sendToAllAsynchronous(new PacketOutProcessRemove(proxyInfo));
        }
    }
}
