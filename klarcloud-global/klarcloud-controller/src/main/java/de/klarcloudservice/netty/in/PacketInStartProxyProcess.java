/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.netty.packets.PacketOutStartProxy;
import de.klarcloudservice.utility.TypeTokenAdaptor;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 26.12.2018
 */

public class PacketInStartProxyProcess implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        final ProxyGroup proxyGroup = configuration.getValue("group", TypeTokenAdaptor.getProxyGroupType());
        if (!KlarCloudController.getInstance().getChannelHandler().isChannelRegistered(proxyGroup.getClient()))
            return;

        final Collection<String> proxies = KlarCloudController.getInstance().getInternalCloudNetwork()
                .getServerProcessManager().getOnlineServers(proxyGroup.getName());
        final Collection<String> waiting = KlarCloudController.getInstance().getCloudProcessOfferService().getWaiting(proxyGroup.getName());

        final int waitingAndOnline = proxies.size() + waiting.size();
        final String id = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().nextFreeProxyID(proxyGroup.getName());
        final String name = proxyGroup.getName() + KlarCloudController.getInstance().getCloudConfiguration().getSplitter() + (Integer.parseInt(id) <= 9 ? "0" : "") + id;

        if (proxyGroup.getMaxOnline() > waitingAndOnline || proxyGroup.getMaxOnline() == -1)
            KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(proxyGroup.getClient(),
                    new PacketOutStartProxy(proxyGroup, name, UUID.randomUUID(), configuration.getConfiguration("pre"), id)
            );
    }
}
