/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packets;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.cloudsystem.InternalCloudNetwork;

import java.util.Collections;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

public final class PacketOutUpdateInternalCloudNetwork extends Packet {
    public PacketOutUpdateInternalCloudNetwork(final InternalCloudNetwork internalCloudNetwork) {
        super("UpdateInternalCloudNetwork", new Configuration().addProperty("networkProperties", internalCloudNetwork), Collections.singletonList(QueryType.COMPLETE), PacketSender.CLIENT);
    }
}
