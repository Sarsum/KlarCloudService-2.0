/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.server.ServerGroup;
import de.klarcloudservice.meta.startup.ServerStartupInfo;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.TypeTokenAdaptor;

import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 29.10.2018
 */

public class PacketInStartGameServer implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        final ServerGroup serverGroup = configuration.getValue("group", TypeTokenAdaptor.getServerGroupType());

        KlarCloudClient.getInstance().getCloudProcessStartupHandler().offerServerProcess(new ServerStartupInfo(
                configuration.getValue("serverProcess", UUID.class), configuration.getStringValue("name"), serverGroup, configuration.getConfiguration("preConfig"), configuration.getIntegerValue("id")
        ));

        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("ServerProcess " + configuration.getStringValue("name") + " has been successfully added to the Client queue");
    }
}
