/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.bootstrap;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.commands.CommandManager;
import de.klarcloudservice.libloader.LibraryLoader;
import de.klarcloudservice.logging.KlarCloudConsoleLogger;
import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.utility.files.FileUtils;
import de.klarcloudservice.utility.time.DateProvider;
import io.netty.util.ResourceLeakDetector;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 23.10.2018
 */

final class KlarCloudClientBootstrap {
    /**
     * Main Method of KlarCloudClient
     *
     * @param args          The given args by the executor
     * @throws Throwable    Will be thrown if an error occurs
     */
    public static synchronized void main(String[] args) throws Throwable {
        final long current = System.currentTimeMillis();

        System.out.println(" Trying to startup KlarCloudClient...");
        System.out.println(" Startup time: " + DateProvider.formatByDefaultFormat(current));

        new LibraryLoader().loadJarFileAndInjectLibraries();

        System.out.println(" Trying to delete old \"logs\" directory...");

        if (Files.exists(Paths.get("klarcloud/logs")))
            FileUtils.deleteFullDirectory(Paths.get("klarcloud/logs"));

        System.out.println(" Old \"logs\" directory has been deleted");

        System.out.println(" Falling back to KlarCloudVersion " + StringUtil.KLARCLOUD_VERSION + "@" + StringUtil.KLARCLOUD_SPECIFICATION);
        System.out.println();

        KlarCloudLibrary.sendHeader();

        final List<String> options = Arrays.asList(args);

        final KlarCloudConsoleLogger klarCloudConsoleLogger = new KlarCloudConsoleLogger(options.contains("--color"));
        final CommandManager commandManager = new CommandManager();

        klarCloudConsoleLogger.info("Disabling ResourceLeakDetector...");

        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.DISABLED);

        klarCloudConsoleLogger.info("ResourceLeakDetector has been disabled");
        klarCloudConsoleLogger.info("Setting System-Properties...");

        System.setProperty("io.netty.maxDirectMemory", "0");
        System.setProperty("io.netty.leakDetectionLevel", "DISABLED");
        System.setProperty("file.encoding", "UTF-8");
        System.setProperty("java.net.preferIPv4Stack", "true");
        System.setProperty("io.netty.recycler.maxCapacity", "0");
        System.setProperty("io.netty.recycler.maxCapacity.default", "0");
        System.setProperty("io.netty.noPreferDirect", "true");
        System.setProperty("client.encoding.override", "UTF-8");

        klarCloudConsoleLogger.info("System-Properties are now ready");

        new KlarCloudClient(klarCloudConsoleLogger, commandManager, options.contains("--ssl"), current);

        klarCloudConsoleLogger.info("Use the command \"help\" for help.");

        String line;
        try {
            while (true) {
                klarCloudConsoleLogger.getConsoleReader().setPrompt("");
                klarCloudConsoleLogger.getConsoleReader().resetPromptLine("", "", 0);

                while ((line = klarCloudConsoleLogger.getConsoleReader().readLine(" " + StringUtil.KLARCLOUD_VERSION + "-" + StringUtil.KLARCLOUD_SPECIFICATION + "@KlarCloudClient > ")) != null && KlarCloudClient.RUNNING) {
                    klarCloudConsoleLogger.getConsoleReader().setPrompt("");

                    if (!commandManager.dispatchCommand(line))
                        klarCloudConsoleLogger.info("Command not found! Use the command \"help\" for help.");
                }
            }
        } catch (final Throwable throwable) {
            StringUtil.printError(KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger(), "Error while handling command", throwable);
        }
    }
}
