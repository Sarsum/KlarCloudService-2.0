/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.versioneering;

import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.utility.files.DownloadManager;
import de.klarcloudservice.utility.files.FileUtils;

import java.io.File;

/**
 * @author _Klaro | Pasqual K. / created on 08.01.2019
 */

public final class VersionUpdater {
    public void update() {
        if (!VersionLoader.getNewestVersion().equalsIgnoreCase(StringUtil.KLARCLOUD_VERSION)) {
            DownloadManager.downloadSilentAndDisconnect("https://dl.klarcloudservice.de/update/latest/" + whereIAm() + ".jar", whereIAm() + "-Update-" + KlarCloudLibrary.THREAD_LOCAL_RANDOM.nextLong() + ".jar");
            FileUtils.deleteOnExit(new File(FileUtils.getInternalFileName()));
            System.exit(1);
        }
    }

    private String whereIAm() {
        return KlarCloudLibraryService.getInstance().getControllerIP() == null ? "KlarCloudService-Controller" : "KlarCloudService-Client";
    }
}
