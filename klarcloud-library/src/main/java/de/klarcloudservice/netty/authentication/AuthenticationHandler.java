/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.authentication;

import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.authentication.enums.AuthenticationType;
import de.klarcloudservice.netty.authentication.interfaces.AuthenticationManager;
import de.klarcloudservice.netty.channel.ChannelHandler;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.AccessChecker;
import io.netty.channel.ChannelHandlerContext;

import java.net.InetSocketAddress;
import java.util.Arrays;

/**
 * @author _Klaro | Pasqual K. / created on 19.10.2018
 */

public class AuthenticationHandler implements AuthenticationManager {

    @Override
    public void handleAuth(AuthenticationType authenticationType, Packet packet, ChannelHandlerContext channelHandlerContext, ChannelHandler channelHandler) {
        final InetSocketAddress inetSocketAddress = ((InetSocketAddress) channelHandlerContext.channel().remoteAddress());

        KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Trying to authenticate [Address=" + inetSocketAddress.getAddress().getHostAddress() + "]...");

        String name = packet.getConfiguration().getStringValue("name");
        switch (authenticationType) {
            case SERVER: {
                if (new AccessChecker().checkString(packet.getConfiguration().getStringValue("key"), KlarCloudLibraryService.getInstance().getKey()).isAccepted()) {
                    channelHandler.closeChannel(name);
                    channelHandler.registerChannel(name, channelHandlerContext);
                    KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("NetworkConnection [Name=" + name + "/Address=" +
                            inetSocketAddress.getAddress().getHostAddress() + "/Service=CloudServer] has been successfully authenticated");

                    channelHandlerContext.channel().writeAndFlush(new Packet(
                            "InitializeCloudNetwork", new Configuration().addProperty("networkProperties", KlarCloudLibraryService.getInstance().getInternalCloudNetwork()), Arrays.asList(QueryType.COMPLETE, QueryType.RESULT), PacketSender.CONTROLLER
                    ));
                } else {
                    KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().warn("Could not authenticate [Address=" + inetSocketAddress.getAddress().getHostAddress() + "]");
                    channelHandlerContext.channel().close();
                }
                break;
            }
            case PROXY: {
                if (new AccessChecker().checkString(packet.getConfiguration().getStringValue("key"), KlarCloudLibraryService.getInstance().getKey()).isAccepted()) {
                    channelHandler.registerChannel(name, channelHandlerContext);
                    KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("NetworkConnection [Name=" + name + "/Address=" +
                            inetSocketAddress.getAddress().getHostAddress() + "/Service=Proxy] has been successfully authenticated");

                    channelHandlerContext.channel().writeAndFlush(new Packet(
                            "InitializeCloudNetwork", new Configuration().addProperty("networkProperties", KlarCloudLibraryService.getInstance().getInternalCloudNetwork()), Arrays.asList(QueryType.COMPLETE, QueryType.RESULT), PacketSender.CONTROLLER
                    ));
                } else {
                    KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().warn("Could not authenticate [Address=" + inetSocketAddress.getAddress().getHostAddress() + "]");
                    channelHandlerContext.channel().close();
                }
                break;
            }
            case INTERNAL: {
                if (new AccessChecker().checkString(packet.getConfiguration().getStringValue("key"), KlarCloudLibraryService.getInstance().getKey()).isAccepted()) {
                    channelHandler.registerChannel(name, channelHandlerContext);
                    KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("NetworkConnection [Name=" + name + "/Address=" +
                            inetSocketAddress.getAddress().getHostAddress() + "/Service=Internal] has been successfully authenticated");

                    channelHandlerContext.channel().writeAndFlush(new Packet(
                            "InitializeCloudNetwork", new Configuration().addProperty("networkProperties", KlarCloudLibraryService.getInstance().getInternalCloudNetwork()), Arrays.asList(QueryType.COMPLETE, QueryType.RESULT), PacketSender.CONTROLLER
                    ));
                } else {
                    KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().warn("Could not authenticate [Address=" + inetSocketAddress.getHostName() + "]");
                    channelHandlerContext.channel().close();
                }
                break;
            }
            default: {
                KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().warn("Could not authenticate [Address=" + inetSocketAddress.getAddress().getHostAddress() + "]");
                channelHandlerContext.channel().close();
                break;
            }
        }
    }
}
