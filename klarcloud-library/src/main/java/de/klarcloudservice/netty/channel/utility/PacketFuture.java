/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.channel.utility;

import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.logging.KlarCloudConsoleLogger;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.utility.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author _Klaro | Pasqual K. / created on 03.01.2019
 */

@Deprecated //Packet Handler must be in an extra thread to make this stable
public class PacketFuture implements Future<Packet> {
    private static final KlarCloudConsoleLogger klarCloudConsoleLogger = KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger();

    private enum PacketFutureState {
        WAITING,
        CANCELLED,
        FAILED,
        DONE
    }

    @Getter
    @Setter
    private Packet value;

    @Setter
    private volatile PacketFutureState packetFutureState = PacketFutureState.WAITING;
    private final Object object = new Object();

    private void await(long timeout, TimeUnit unit) {
        synchronized (object) {
            final long end = System.currentTimeMillis() + unit.toMillis(timeout);
            do {
                try {
                    object.wait(end - System.currentTimeMillis());
                } catch (final InterruptedException ex) {
                    StringUtil.printError(klarCloudConsoleLogger, "Thread error", ex);
                }
            } while (packetFutureState.equals(PacketFutureState.WAITING) && System.currentTimeMillis() < end);

            if (packetFutureState.equals(PacketFutureState.WAITING))
                StringUtil.printError(klarCloudConsoleLogger, "Timeout in packetFuture", new TimeoutException());
        }
    }

    private void await() {
        synchronized (object) {
            do {
                try {
                    object.wait();
                } catch (final InterruptedException ex) {
                    StringUtil.printError(klarCloudConsoleLogger, "Thread error", ex);
                }
            } while (packetFutureState.equals(PacketFutureState.WAITING));
        }
    }

    private void hasFailed() {
        if (packetFutureState.equals(PacketFutureState.FAILED) || packetFutureState.equals(PacketFutureState.CANCELLED)) {
            throw new IllegalStateException();
        }
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        synchronized (object) {
            if (isDone())
                return false;

            this.packetFutureState = PacketFutureState.CANCELLED;
            object.notifyAll();
        }
        return true;
    }

    @Override
    public boolean isCancelled() {
        return packetFutureState.equals(PacketFutureState.CANCELLED);
    }

    @Override
    public boolean isDone() {
        return !packetFutureState.equals(PacketFutureState.WAITING);
    }

    @Override
    public Packet get() {
        synchronized (object) {
            await();
            hasFailed();
        }

        return value;
    }

    @Override
    public Packet get(long timeout, TimeUnit unit) {
        synchronized (object) {
            await(timeout, unit);
            hasFailed();
            return value;
        }
    }

    public boolean set(Packet value) {
        synchronized (object) {
            if (isDone())
                return false;

            this.packetFutureState = PacketFutureState.DONE;
            this.value = value;
            object.notifyAll();
        }
        return true;
    }

    public Packet syncUninterruptedly() {
        synchronized (object) {
            return get();
        }
    }

    public Packet syncUninterruptedly(long timeOut, TimeUnit timeUnit) {
        synchronized (object) {
            return get(timeOut, timeUnit);
        }
    }
}
