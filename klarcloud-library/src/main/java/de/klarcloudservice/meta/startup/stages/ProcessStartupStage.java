/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.startup.stages;

/**
 * @author _Klaro | Pasqual K. / created on 24.11.2018
 */

public enum ProcessStartupStage {
    WAITING,
    COPY,
    PREPARING,
    START,
    DONE;

    ProcessStartupStage() {
    }
}
