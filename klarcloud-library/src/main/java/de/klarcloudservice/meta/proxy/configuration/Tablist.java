/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.proxy.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author _Klaro | Pasqual K. / created on 15.01.2019
 */

@AllArgsConstructor
@Getter
public final class Tablist {
    private boolean activated;
    private int refreshPerSecond;
    private String upper_line, lower_line;
}
